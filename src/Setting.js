import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import LiffContextProvider from './contexts/LiffContext'

const Setting = ({
  children
}) => {
  return (
    <BrowserRouter>
      <LiffContextProvider>
        {children}
      </LiffContextProvider>
    </BrowserRouter>
  )
}

export default Setting
