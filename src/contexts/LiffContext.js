import React, { useState, useEffect, useContext } from 'react'
import { useAsync } from 'react-use'
import liff from '@line/liff'
import { Skeleton, Result } from 'antd'
const LIFFID = window._env.LIFFID || '1654447478-Rx5vevNd'
const LiffContext = React.createContext({
  profile: null
})
const LiffContextProvider = ({
  children
}) => {
  // init liff
  const {
    loading,
    error,
    value: profile
  } = useAsync(async () => {
    await liff.init({
      liffId: LIFFID
    })
    // force login
    if (!liff.isLoggedIn()) {
      liff.login({
        redirectUri: `${window.location.protocol}//${window.location.host}/auth/line?next=${window.location.pathname}`
      })
    }
    return liff.getProfile()
  }, [LIFFID])
  if (error) {
    return <Result status="Get liff profile error" />
  }
  if (loading) {
    return <Skeleton />
  }

  const value = {
    profile
  }
  return (
    <LiffContext.Provider value={value}>
      {children}
    </LiffContext.Provider>
  )
}

const useLiff = () => {
  const context = useContext(LiffContext)
  if (!context) {
    throw new Error('useLiff cannot use outside LiffContetProvider')
  }
  return context
}

export default LiffContextProvider
export {
  useLiff
}
