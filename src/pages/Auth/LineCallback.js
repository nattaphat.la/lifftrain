import React, { useEffect } from 'react'
import { useLocation, useHistory } from 'react-router'
import qs from 'qs'
const LineCallback = () => {
  const {
    search,
    pathname
  } = useLocation()
  const history = useHistory()
  const query = qs.parse(search, {
    ignoreQueryPrefix: true
  })
  // get query string and redirect
  useEffect(() => {
    if (query.next && query.next !== pathname) {
      history.push(query.next)
    }
  }, [query.next, history, pathname])
  return (
    <div>
      should redirect
    </div>
  )
}

export default LineCallback
