import React, { useEffect } from 'react'
import { useLiff } from '../../contexts/LiffContext'
import liff from '@line/liff/dist/lib'
import { useAsync } from 'react-use'

const Home = () => {
  const {
    value
  } = useAsync(async () => {
    return liff.getDecodedIDToken()
  })
  const {
    profile
  } = useLiff()
  return (
    <div>
      {JSON.stringify(profile)}
      {JSON.stringify(value || {})}
    </div>
  )
}

export default Home
