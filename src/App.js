import React from 'react';
import './App.less';
import Setting from './Setting';
import { Switch, Route } from 'react-router';
import Home from './pages/Home/Home';
import LineCallback from './pages/Auth/LineCallback';
import { Result } from 'antd';


const App = () => (
  <Setting>
    <Switch>
      <Route path="/" exact>
        <Home />
      </Route>
      <Route path="/auth/line" exact>
        <LineCallback />
      </Route>
      <Route >
        <Result status="404" />
      </Route>
    </Switch>
  </Setting>
);

export default App;